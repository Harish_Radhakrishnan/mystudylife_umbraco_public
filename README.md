mystudylife.com
===============
The My Study Life brochure/tour site.

Umbraco 7 application.

## Getting Started
### Prerequisities
- Visual Studio 2019 or Rider 2020
- .NET Framework SDK 4.5
- IIS Express 
- Node 8.11 (pipeline) or above (tested with 12.16)
- [Yarn](https://yarnpkg.com/)
- SQL Server 2016 or above

> The CI Pipelines in Azure DevOps contain complete dependency/build steps (`azure-pipelines.yml`).

### Configuration
Configuration is managed using the standard Umbraco approach.

In addition, `rewriteMaps.config` and `rewriteRules.config` contain URL rewriting configuration.

### Local Environment Setup
> You must have a user account in production in order to log in after restoring the production database.

1. Create your local database by taking a backup of the production database (this is important as it contains the structure for the website). Setup authentication using either Windows or SQL auth.
2. Copy the contents of the production `media` folder to `src\MyStudyLife.Umbraco\media` (the numbers of the folders must match the ids in the database).
3. Under the `src\MyStudyLife.Umbraco\config` directory, copy `connectionStrings.config.tmpl` to `connectionStrings.config` and update the connection string to point to the restored local database.
3. Install local dependencies by running `yarn` in the `src\MyStudyLife.Umbraco` directory.
4. Still in the `src\MyStudyLife.Umbraco` directory, compile local dependencies (LESS and JS) in MyStudyLife.Accounts by running `node_modules\.bin\gulp min`.
5. Run the application from Visual Studio or Rider.

> If using Visual Studio, the gulp tasks can be run using the Task Runner Explorer window. 

### Working with Accounts LESS/JS
When developing, the LESS is compiled in the browser which enables changes to be immediately visible after a refresh and to also display any error messages in the browser.

The JS can be continuously compiled by watching for file updates by running `node_modules\.bin\gulp watch:js`.

The JS is compiled to `bundle.js` (and `.min` varient) and the LESS to `main.min.css` which are included in the `.csproj` as `Content`. Removing the references will result in the files missing when deploying.

### Deploying Changes
Any changes made in the Umbraco backoffice such as adding document types, properties, media (images) or changing content must be manually re-applied to production as part of the deployment process.

All other changes follow a standard web deploy pattern (see release pipeline in Azure DevOps), taking care not to delete the `media` folder.

### Commit Messages
- Use the present tense ("Fix bug" not "Fixed bug")
- Use the imperative mood ("Add dashboard setting to user model..." not "Adds dashboard setting to user model...")
- Limit the first line to 72 characters or less (treat emojis as a single character)
- Reference JIRA issues

## Troubleshooting
If a backoffice user password is forgotten, enable password reset as per the article below. You also need to make sure the `mailSettings` are completed in `Web.Config` under `system.net` in order for a password reset email to send

https://our.umbraco.com/documentation/reference/config/umbracosettings/#security