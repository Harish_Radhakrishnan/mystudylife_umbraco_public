﻿using System;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using MyStudyLife.Umbraco.Utility;
using Umbraco.Web;

namespace MyStudyLife.Umbraco {
    public class MyStudyLifeUmbracoApplication : UmbracoApplication {
        private readonly string _liveHostName, _username, _password;

        public MyStudyLifeUmbracoApplication() {
            var environment = TryGetAppSetting<string>("Environment");

            if (!String.IsNullOrWhiteSpace(environment) && String.Equals(environment, "production", StringComparison.OrdinalIgnoreCase)) {
                _liveHostName = TryGetAppSetting("LiveHostName", "www.mystudylife.com");

                this.BeginRequest += EnsureProductionUrl;

                return;
            }

            if (TryGetAppSetting<bool>("BasicAuth.IsEnabled")) {
                _username = TryGetAppSetting<string>("BasicAuth.Username") ?? "preview";
                _password = TryGetAppSetting<string>("BasicAuth.Password") ?? String.Concat("mystudylife", DateTime.Now.Year);

                this.BeginRequest += Authenticate;
            }
        }

        protected override void OnApplicationStarted(object sender, EventArgs e) {
            base.OnApplicationStarted(sender, e);

            GlobalFilters.Filters.Add(new HandleExceptionAttribute());
            GlobalFilters.Filters.Add(new MyStudyLife.Umbraco.Utility.RequireHttpsAttribute());

            RouteTable.Routes.MapRoute(
                "AppRedirect",
                "app",
                new { controller = "App", action = "DoRedirect" }
            );
        }
        
        private void EnsureProductionUrl(object sender, EventArgs e) {
            if (!String.Equals(Request.ServerVariables["HTTP_HOST"], _liveHostName)) {
                Response.Redirect(String.Concat("https://", _liveHostName, Request.RawUrl));
            }
        }

        private void Authenticate(object sender, EventArgs e) {
            if (Request.IsLocal) {
                return;
            }

            bool authorized = false;

            string authHeader = Request.Headers["Authorization"];

            if (!String.IsNullOrEmpty(authHeader) && authHeader.StartsWith("Basic")) {
                string encodedCredentials = authHeader.Substring(6).Trim();

                // That's the right encoding                        
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string credentials = encoding.GetString(Convert.FromBase64String(encodedCredentials));
                int separator = credentials.IndexOf(':');

                if (credentials.Substring(0, separator) == _username && credentials.Substring(separator + 1) == _password) {
                    authorized = true;
                }
            }

            if (!authorized) {
                Response.StatusCode = 401;
                Response.AddHeader("WWW-Authenticate", $"Basic realm=\"{Request.Url.Host}\"");
                Response.Flush();
                Response.Close();
            }
        }

        private T TryGetAppSetting<T>(string key, T defaultValue = default(T)) {
            var appSettings = WebConfigurationManager.AppSettings;

            var settings = appSettings.GetValues(key);

            if (settings == null) {
                return defaultValue;
            }

            var setting = settings.FirstOrDefault();

            if (setting == null) {
                return defaultValue;
            }

            return (T)Convert.ChangeType(setting, typeof (T));
        }
    }
}