﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.ApplicationInsights;
using Mindscape.Raygun4Net;

namespace MyStudyLife.Umbraco.Utility {
    public sealed class HandleExceptionAttribute : FilterAttribute, IExceptionFilter {
        private readonly RaygunClient _raygunClient = new RaygunClient();
        private readonly TelemetryClient _applicationInsightsClient = new TelemetryClient();

        public void OnException(ExceptionContext filterContext) {
            if (filterContext.ExceptionHandled || !filterContext.HttpContext.IsCustomErrorEnabled) {
                return;
            }

            var ex = filterContext.Exception;
            var httpEx = ex as HttpException;

            if (httpEx != null && httpEx.GetHttpCode() != 500) {
                return;
            }
            
            if (ex is UnauthorizedAccessException) {
                return;
            }

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

            filterContext.Result = new PartialViewResult {
                ViewName = "~/Views/Error500.cshtml"
            };

            _raygunClient.SendInBackground(ex);
            _applicationInsightsClient.TrackException(ex);
        }
    }
}
