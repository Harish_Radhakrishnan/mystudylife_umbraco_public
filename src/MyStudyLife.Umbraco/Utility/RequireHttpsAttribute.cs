﻿using System;
using System.Net;
using System.Web.Mvc;

namespace MyStudyLife.Umbraco.Utility {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RequireHttpsAttribute : System.Web.Mvc.RequireHttpsAttribute {
        public override void OnAuthorization(AuthorizationContext filterContext) {
            if (filterContext == null) throw new ArgumentNullException(nameof(filterContext));

            if (filterContext.HttpContext != null && filterContext.HttpContext.Request.IsLocal) {
                return;
            }

            base.OnAuthorization(filterContext);
        }

        protected override void HandleNonHttpsRequest(AuthorizationContext filterContext) {
            filterContext.Result = new HttpsRedirectResult();
        }
    }

    /// <summary>
    ///     Redirects the current non https result to https, using the appropriate
    ///     status code depending on the method.
    /// </summary>
    public class HttpsRedirectResult : ActionResult {
        public HttpsRedirectResult() { }

        public override void ExecuteResult(ControllerContext context) {
            var request = context.HttpContext.Request;
            var response = context.HttpContext.Response;

            string url = String.Concat("https://", request.Url.Host, request.Url.PathAndQuery);

            // Moved = 301 which is permanent vs Redirect = 302 which is temporary
            var statusCode = (
                String.Equals(request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase) ||
                String.Equals(request.HttpMethod, "HEAD", StringComparison.OrdinalIgnoreCase)
            ) ? HttpStatusCode.Moved : HttpStatusCode.RedirectKeepVerb;

            response.StatusCode = (int)statusCode;
            response.RedirectLocation = url;
        }
    }
}