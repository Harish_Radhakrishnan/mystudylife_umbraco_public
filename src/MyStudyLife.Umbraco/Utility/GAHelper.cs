﻿using System;

namespace MyStudyLife.Umbraco.Utility {
    public static class GAHelper {
        /// <summary>
        ///     Appends a utm_medium query param to the string if it does not
        ///     exist.
        /// </summary>
        /// <remarks>
        ///     This is crude but should work for our purposes.
        /// </remarks>
        public static string UrlWithMedium(string url, string medium) {
            if (String.IsNullOrEmpty(medium)) throw new ArgumentNullException(nameof(medium));

            if (url.Contains("utm_source=")) {
                return $"{url}&utm_medium={medium}";
            }
            else {
                return $"{url}{(url.Contains("?") ? "&" : "?")}utm_source=mystudylife.com&utm_medium={medium}";
            }
        }
    }
}