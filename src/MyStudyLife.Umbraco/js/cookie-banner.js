(function (window, $) {
    var CONSENT_COOKIE_NAME = 'msl.cookie-consent';

    var COOKIE_BANNER_HTML =
        '<section class="cookie-banner">\n' +
        '    <div class="cookie-banner-inner">\n' +
        '        <p>We use cookies to personalise content and ads, to provide social media features and analyse our traffic. You consent to cookies if you continue to use our website.</p>\n' +
        '        <div class="cookie-banner-consent">\n' +
        '            <button class="light cookie-banner-agree">I agree</button>\n' +
        '            <a class="cookie-banner-learn-more" href="https://www.iubenda.com/privacy-policy/70328380/cookie-policy">Learn More</a>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</section>';

    function addFacebookPixel() {
        (function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        })(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        
        fbq('init', '362649814068623');
        fbq('track', 'PageView');
    }
    
    function addHubspot() {
        var hubspotScript = document.createElement('script');
        hubspotScript.async = true;
        hubspotScript.src = 'https://js.hs-scripts.com/3108766.js';
        document.getElementsByTagName('script')[0].insertBefore(hubspotScript, null);
    }
    
    function onConsent() {
        addFacebookPixel();
        addHubspot();
    }
    
    if (!window.msl) window.msl = {};
     
    msl.gainCookieConsent = function (cookieDomain) {
        var cookie = Cookies.get(CONSENT_COOKIE_NAME);

        if (!cookie) {
            $(function () {
                var $cookieBanner = $(COOKIE_BANNER_HTML);

                $cookieBanner.on('click', '.cookie-banner-agree', function (e) {
                    e.preventDefault();

                    Cookies.set(CONSENT_COOKIE_NAME, '1', {expires: 730, domain: cookieDomain});

                    $cookieBanner.remove();

                    onConsent();
                });

                $cookieBanner.appendTo(document.body);
            });
        } else {
            onConsent();
        }
    };
    
})(window, jQuery);