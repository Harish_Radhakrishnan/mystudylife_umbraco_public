﻿(function ($, scrollReveal) {
    window.srInstance = new scrollReveal();

    function setupResponsiveNav() {
        var body = $('body'),
            navToggle = $('#nav-toggle');

        function toggleNavActive(e) {
            e.preventDefault();
            e.stopPropagation();

            body.toggleClass('nav-active');
        }

        navToggle.click(toggleNavActive);

        body.click(function (e) {
            if (body.hasClass('nav-active') && !$(e.target).parents('header').length) {
                toggleNavActive(e);
            }
        })
    };

    function setupBgParallax() {
        var bgParallaxEl = $('.bg-parallax');

        if (bgParallaxEl.length && typeof window.requestAnimationFrame !== 'undefined') {
            window.addEventListener('scroll', function() {
                window.requestAnimationFrame(function() {
                    bgParallaxEl.css('backgroundPositionY', (window.pageYOffset - bgParallaxEl.offset().top) / 8);
                });
            });
        }
    };

    function setupFillScreen() {
        // Blog, text and error pages
        var $el = $('section.fill-screen');

        function setMinimumHeight() {
            $el.css('min-height', window.innerHeight - ($('body > header').height() + $('body > footer').height()));
        }

        if (typeof window.addEventListener != 'undefined') {
            window.addEventListener('resize', setMinimumHeight);
        }

        setMinimumHeight();
    };


    function setupReviews() {
        var MIN_SWIPE_VELOCITY = 0.35,
            reviewsEl = $('#feedback .reviews');

        if (!reviewsEl.length) {
            return;
        }

        var reviewCount = reviewsEl.find('article').length,
            maxReviewIndex = reviewCount - 1;

        function setNavState(canPrev, canNext) {
            var $l = $('#feedback .touch-nav .l'),
                $r = $('#feedback .touch-nav .r');

            if (canPrev) {
                $l.removeClass('disabled');
            }
            else {
                $l.addClass('disabled');
            }

            if (canNext) {
                $r.removeClass('disabled');
            }
            else {
                $r.addClass('disabled');
            }
        };

        function translate(change) {
            var reviewIndex = Number(reviewsEl.attr('data-reviews-current'));

            if (isNaN(reviewIndex)) {
                reviewIndex = Math.round((reviewCount / 2) - 1);
            }

            var displayAtOnce = window.innerWidth <= 480 ? 1 : (window.innerWidth <= 768 ? 2 : 3),
                maxDisplayReviewIndex = maxReviewIndex - (displayAtOnce - 1);

            if (change == 1) {
                reviewIndex += displayAtOnce;

                if (reviewIndex > maxDisplayReviewIndex) {
                    reviewIndex = maxDisplayReviewIndex;
                }
            }
            else if (change == -1 && reviewIndex > 0) {
                reviewIndex -= displayAtOnce;

                if (reviewIndex < 0) {
                    reviewIndex = 0;
                }
            }

            setNavState(reviewIndex > 0, reviewIndex < maxDisplayReviewIndex);

            reviewsEl
                .css({
                    transform: 'translate3d(-' + reviewIndex * (10 / 3) + '%, 0, 0)'
                })
                .attr('data-reviews-current', reviewIndex);
        };

        function isAutoTranslatePaused() {
            return $('#feedback .reviews-wrapper').hasClass('paused');
        };

        function autoTranslateReviews() {
            if (window.reviewsTimeout) {
                clearTimeout(window.reviewsTimeout);
            }

            if (!isAutoTranslatePaused()) {
                var reviewIndex = Number(reviewsEl.attr('data-reviews-current'));

                if (isNaN(reviewIndex)) {
                    reviewIndex = Math.round((reviewCount / 2) - 1);
                }

                var displayAtOnce = window.innerWidth <= 480 ? 1 : (window.innerWidth <= 768 ? 2 : 3);

                var maxDisplayReviewIndex = maxReviewIndex - (displayAtOnce - 1);

                if (maxDisplayReviewIndex === reviewIndex) {
                    reviewIndex = 0;
                }
                else {
                    reviewIndex++;
                }

                setNavState(reviewIndex > 0, reviewIndex < maxDisplayReviewIndex);

                reviewsEl
                    .css({
                        transform: 'translate3d(-' + reviewIndex * (10 / 3) + '%, 0, 0)'
                    })
                    .attr('data-reviews-current', reviewIndex);
            }

            window.reviewsTimeout = setTimeout(autoTranslateReviews, 3000);
        }

        // Mainly for touch, mouse is handled with hover event.
        function pauseAutoTranslateReviews() {
            if (window.pauseReviewsTimeout) {
                clearTimeout(window.pauseReviewsTimeout);
            }

            if (window.reviewsTimeout) {
                clearTimeout(window.reviewsTimeout);
            }

            window.pauseReviewsTimeout = setTimeout(autoTranslateReviews, 10000);
        }

        $('[data-reviews-change]').on('click', function(e) {
            e.preventDefault();

            translate($(this).attr('data-reviews-change'));

            pauseAutoTranslateReviews();
        });

        $('#feedback .reviews-wrapper').hover(
            function() {
                $(this).addClass('paused');
            },
            function() {
                $(this).removeClass('paused');
            }
        );

        new Hammer(reviewsEl[0])
            .on('swipe', function(e) {
                pauseAutoTranslateReviews();

                if (MIN_SWIPE_VELOCITY <= Math.abs(e.velocityX)) {
                    if (e.direction === 4) {
                        translate(-1);
                    }
                    else if (e.direction === 2) {
                        translate(1);
                    }
                }
            });

        translate(0);

        if (!window.srInstance.isDisabled()) {
            var oldFn = window.srInstance.options.complete;

            window.srInstance.options.complete = function onScrollRevealComplete(el) {
                if (el && el.classList.contains('review')) {
                    if (isNaN(window.revealedReviews)) {
                        window.revealedReviews = 1;
                    }
                    else {
                        window.revealedReviews++;
                    }

                    if (window.revealedReviews === reviewCount) {
                        setTimeout(autoTranslateReviews, 1500);
                    }
                }

                if (typeof oldFn === 'function') {
                    oldFn(el);
                }
            }
        }
        else {
            setTimeout(autoTranslateReviews, 3000);
        }
    };

    function setupTourNav() {
        var $tourNav = $('nav#tour-nav');

        if (!$tourNav.length) {
            return;
        }

        var tourItems = $tourNav.find('a').map(function() {
            return $(this).attr('data-item');
        }).toArray();

        function setTourItemState(currItem, pushState, isPopState) {
            var attrCss = '[data-item="' + currItem + '"]',
                $items = $('nav#tour-nav a, .tour-item');

            $items.not(attrCss).removeClass('current');
            $items.filter(attrCss).addClass('current');

            var tourNavX = $tourNav.offset().top;

            if (tourNavX < $(window).scrollTop() || isPopState) {
                $(window).scrollTop(tourNavX);
            }

            if (!window.srInstance.isDisabled()) {
                window.srInstance._scrollPage();
            }

            if (pushState && (typeof history !== 'undefined' && history.pushState)) {
                var title = $tourNav.find('a' + attrCss).text() + ' - Tour - My Study Life';

                history.pushState(
                    {
                        msl: true
                    },
                    title,
                    '/tour/' + currItem
                );

                document.title = title;
            }
        };

        $tourNav.find('a').on('click', function(e) {
            e.preventDefault();

            setTourItemState($(this).attr('data-item'), true);
        });

        $('.next-tour-item').on('click', function(e) {
            e.preventDefault();

            var i = tourItems.indexOf($tourNav.find('a.current').attr('data-item'));

            setTourItemState(tourItems[i + 1], true);
        });

        if (typeof window.addEventListener !== 'undefined') {
            window.addEventListener('popstate', function(e) {
                // Fixes bug in Safari where triggered on load
                if (e.state && e.state.msl === true) {
                    var path = window.location.pathname,
                        item = path.substr(1).split('/')[1];

                    if (item.indexOf('?') > -1) {
                        item = item.split('?')[0];
                    }

                    setTourItemState(item, false, true);
                }
            });
        }
    };

    function setupSchoolStudentReviews() {
        var $reviews = $('#students .reviews');

        if (!$reviews.length) {
            return;
        }

        var $reviewToCenter = $reviews.children().eq(1);

        function offsetReviews() {
            $reviews.css('top', ($reviews.parent().height() / 2) - ($reviewToCenter.position().top + ($reviewToCenter.outerHeight(true) / 2)));
        };

        // The setTimeout is required for correct sizing
        setTimeout(offsetReviews);

        window.addEventListener('resize', offsetReviews, false);
    };

    function setupHubspotForms() {
        $('body').on('hsvalidatedsubmit', '.hs-form', function (e) {
            var $form = $(e.target),
                $toReplace = $('[data-replaceonsubmit-form=\'' + $form.attr('data-form-id') + '\']'),
                replacementHtml = $('#' + $toReplace.attr('data-replaceonsubmit-content')).html();
            
            $toReplace.hide();
            $toReplace.parent().append(replacementHtml);

            if (fbq) {
                fbq('track', 'Lead');
            }

            if (ga) {
                ga('send', 'event', 'Lead Form', 'Submitted');
            }
        });
    };

    function setupAboutUs() {
        // Based on a 2000px square map
        var FLEET_OFFSET = {
                x: 1292,
                y: 1151
            },
            $aboutUs = $('#us');

        if (!$aboutUs.length) {
            return;
        }

        var $map = $aboutUs.find('.map'),
            $iframe = $map.find('iframe'),
            $marker = $aboutUs.find('.img .marker');

        function setMapOffset() {
            var markerOffset = $marker.offset(),
                sectionOffset = $aboutUs.offset(),
                x = (markerOffset.left - sectionOffset.left) + ($marker.width() / 2),
                y = (markerOffset.top - sectionOffset.top) + $marker.height();

            $iframe.css('left', x - FLEET_OFFSET.x);
            $iframe.css('top', y - FLEET_OFFSET.y);
        };

        if (typeof window.addEventListener != 'undefined') {
            window.addEventListener('resize', setMapOffset);
        }

        setMapOffset();
    };

    $(function () {
        setupResponsiveNav();
        setupBgParallax();
        setupFillScreen();
        setupReviews();
        setupSchoolStudentReviews();
        setupHubspotForms();
        setupAboutUs();
        setupTourNav();

        var ga = window.ga;

        $('a').click(function(e) {
            if (e.isDefaultPrevented() || typeof ga !== 'function') {
                return;
            }

            var el = this;

            // Only track non MSL links
            if (el.host.indexOf('mystudylife.com') === (el.host.length - 15)) {
                return;
            }

            var openInCurrentWindow = el.target && el.target !== '_self',
                href = el.href;

            if (openInCurrentWindow) {
                e.preventDefault();
            }

            function loadPage() {
                document.location = href;
            }

            ga('send', {
                'hitType': 'event',
                'eventCategory': 'outbound',
                'eventAction': 'link',
                'eventLabel': href,
                'hitCallback': loadPage
            });

            // If Google takes too long, redirect anyway
            setTimeout(loadPage, 1000);
        });
    });
})(jQuery, scrollReveal);