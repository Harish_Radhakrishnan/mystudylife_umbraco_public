﻿using System;
using System.Globalization;
using System.Web;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace MyStudyLife.Umbraco.Models {
    public sealed class BlogPostModel : BaseModel {
        public HtmlString ContentHtml { get; private set; }

        public string ContentShort { get; private set; }
        
        public string PostedBy { get; private set; }

        public DateTime PostedOn { get; private set; }

        public string Meta {
            get { return String.Format("Posted by {0} on {1:D}", this.PostedBy, this.PostedOn); }
        }

        public BlogPostModel(RenderModel model) : base(model) {}
        public BlogPostModel(IPublishedContent content, CultureInfo culture) : base(content, culture) {}

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            this.ContentHtml = content.GetPropertyValue<HtmlString>("contentHtml");
            this.ContentShort = library.StripHtml(content.GetPropertyValue<string>("contentHtml")).Truncate(160);
            this.PostedBy = content.CreatorName;
            this.PostedOn = content.GetPropertyValue("postDate", content.CreateDate);
        }
    }
}