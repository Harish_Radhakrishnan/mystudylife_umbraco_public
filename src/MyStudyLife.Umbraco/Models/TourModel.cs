﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace MyStudyLife.Umbraco.Models {
    public class TourModel : CoreModel {
        public IEnumerable<TourItemModel> TourItems { get; private set; }
        
        public TourModel(IPublishedContent content, CultureInfo culture): base(content, culture) {
            
        }

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            var currentItem = content.Children.SingleOrDefault(x => x.Id == UmbracoContext.Current.PageId);

            string currentItemUrlName = (currentItem ?? content.Children.First()).UrlName;

            this.TourItems = content.Children.Select(
                x => new TourItemModel(x, this) {
                    IsCurrent = x.UrlName == currentItemUrlName
                }
            );
        }
    }

    public class TourItemModel {
        public TourModel TourModel { get; private set; }

        public IPublishedContent Content { get; private set; }

        public string TemplatePath { get; private set; }

        public string Icon { get; private set; }

        public string NextItemUrl { get; private set; }

        public string NextItemName { get; private set; }

        public bool HasNextItem {
            get { return this.NextItemUrl != null; }
        }

        public bool IsCurrent { get; set; }

        public TourItemModel(IPublishedContent content, TourModel parent) {
            this.TourModel = parent;
            this.Content = content;
            this.TemplatePath = ApplicationContext.Current.Services.FileService.GetTemplate(content.TemplateId).Name;
            this.Icon = content.GetPropertyValue<string>("icon");

            var nextItem = content.Next();

            if (nextItem != null) {
                this.NextItemUrl = nextItem.Url;
                this.NextItemName = nextItem.Name;
            }
        }
    }
}