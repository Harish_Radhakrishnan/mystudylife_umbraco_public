﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace MyStudyLife.Umbraco.Models {
    public class BaseModel : RenderModel {

        /// <summary>
        ///     The title of the html document.
        /// </summary>
        public virtual string PageTitle { get; private set; }

        public virtual string PageDescription { get; private set; }

        public bool HasPageDescription {
            get { return this.PageDescription != null; }
        }

        public bool NoIndex { get; private set; }

        public virtual string ContentTitle { get; private set; }

        #region Devices
        // "like" in regex as Windows Phone will add "like Android" and "like iPhone" in certain cases
        // by ensuring the "Android" or "iPhone" is not preceded by like, we know it's not a Windows Phone

        // ReSharper disable once InconsistentNaming
        private static readonly Regex iDeviceUserAgentRegex = new Regex("(?<!like )(?:iPhone|iPod)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex AndroidUserAgentRegex = new Regex(@"(?<!like )(?:Android)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public bool IsiPhoneOriPod { get; private set; }

        public bool IsAndroid { get; private set; }

        public bool IsWindowsPhone { get; private set; }

        // ReSharper disable once InconsistentNaming
        public string iOSAppUrl { get; private set; }

        public string AndroidAppUrl { get; private set; }

        public string WindowsPhoneAppUrl { get; private set; }

        public string WindowsAppUrl { get; private set; }

        public string ChromeAppUrl { get; private set; }

        public bool IsDeviceWithApp => (this.IsiPhoneOriPod || this.IsAndroid || this.IsWindowsPhone);

        public bool ShowSignInButton => !this.IsDeviceWithApp;

        public string AppVersion { get; private set; }

        #endregion

        public BaseModel(RenderModel model) : this(model.Content, model.CurrentCulture) {}

        public BaseModel(IPublishedContent content, CultureInfo culture) : this(content, culture, false) { }

        protected BaseModel(RenderModel model, bool noInit) : this(model.Content, model.CurrentCulture, noInit) { }

        protected BaseModel(IPublishedContent content, CultureInfo culture, bool noInit) : base(content, culture) {
            if (!noInit) this.Initialize();
        }

        private BaseModel() : base(null) { }

        private bool _initialized;

        protected void Initialize() {
            if (_initialized) {
                throw new Exception("Already initialized");
            }

            this.PopulateFromContentInternal(this.Content, this.CurrentCulture);

            this.AppVersion = FileVersionInfo.GetVersionInfo(this.GetType().Assembly.Location).ProductVersion;

            _initialized = true;
        }

        private void PopulateFromContentInternal(IPublishedContent content, CultureInfo culture) {
            var title = content.GetPropertyValue<string>("pageTitle", content.Name);

            this.PageTitle = content.Level > 1 ? String.Concat(title, " - My Study Life") : title;
            this.PageDescription = content.GetPropertyValue<string>("metaDescription");
            this.NoIndex = content.GetPropertyValue<bool>("noIndex");

            this.ContentTitle = title;

            string userAgent = HttpContext.Current.Request.UserAgent;

            if (userAgent != null) {
                this.IsWindowsPhone = userAgent.Contains("Windows Phone");

                // In the most recent versions of Windows Phone 8.1, WP has dropped the "like" from
                // "like Android" (although still has "like iPhone").
                if (!this.IsWindowsPhone) {
                    this.IsiPhoneOriPod = iDeviceUserAgentRegex.IsMatch(userAgent);
                    this.IsAndroid = AndroidUserAgentRegex.IsMatch(userAgent);
                }
            }

            this.iOSAppUrl = content.GetPropertyValue<string>("iOSAppUrl", true);
            this.AndroidAppUrl = content.GetPropertyValue<string>("androidAppUrl", true);
            this.WindowsPhoneAppUrl = content.GetPropertyValue<string>("windowsPhoneAppUrl", true);
            this.WindowsAppUrl = content.GetPropertyValue<string>("windowsAppUrl", true);
            this.ChromeAppUrl = content.GetPropertyValue<string>("chromeAppUrl", true);

            this.PopulateFromContent(content, culture);
        }

        protected virtual void PopulateFromContent(IPublishedContent content, CultureInfo culture) {

        }

        public string UrlWithDomain(string path) {
            var request = HttpContext.Current.Request;

            return String.Concat(request.Url.GetLeftPart(UriPartial.Authority), "/", path.TrimStart('/'));
        }

        public string CacheBust(string path)
            // In the future we may be able to remove this by deploying files with a SHA checksum
            => String.Concat(path, "?v=", AppVersion.Replace(".", ""));
    }
}