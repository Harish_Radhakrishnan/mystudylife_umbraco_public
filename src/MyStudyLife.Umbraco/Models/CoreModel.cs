﻿using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyStudyLife.Umbraco.Models {
    public class CoreModel : BaseModel {
        private string _contentTitle;

        public override string ContentTitle {
            get { return _contentTitle; }
        }

        public string HeroText { get; private set; }

        public CoreModel(IPublishedContent content, CultureInfo culture)
            : base(content, culture) {

        }

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            this._contentTitle = content.GetPropertyValue<string>("heroTitle", content.Name);
            this.HeroText = content.GetPropertyValue<string>("heroText");
        }
    }
}