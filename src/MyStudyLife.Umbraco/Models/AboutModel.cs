﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyStudyLife.Umbraco.Models {
    public sealed class AboutModel : CoreModel {
        public HtmlString HistoryHtml { get; private set; }

        public AboutModel(IPublishedContent content, CultureInfo culture) : base(content, culture) {
            
        }

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            this.HistoryHtml = content.GetPropertyValue<HtmlString>("historyHtml");
        }
    }
}