﻿using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using umbraco;
using Umbraco.Core.Models;
using Umbraco.Web;
using System;
using System.Web.Configuration;

namespace MyStudyLife.Umbraco.Models {
    public class SchoolsModel : CoreModel {
        public IReadOnlyCollection<ReviewModel> StudentReviews { get; private set; }

        public string CustomBrandingScreenshot { get; private set; }

        public string RecaptchaSiteKey => WebConfigurationManager.AppSettings["Recaptcha.SiteKey"];

        public SchoolsModel(IPublishedContent content, CultureInfo culture): base(content, culture) { }

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            this.StudentReviews = content.AncestorOrSelf(1).Descendants("review").RandomOrder().Select(x => new ReviewModel(x)).Take(3).ToList();

            this.CustomBrandingScreenshot = content.GetPropertyValue<string>("customBrandingScreenshot");

            if (!String.IsNullOrWhiteSpace(this.CustomBrandingScreenshot)) {
                this.CustomBrandingScreenshot += "?width=538";
            }
        }
    }
}