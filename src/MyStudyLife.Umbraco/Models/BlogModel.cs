﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace MyStudyLife.Umbraco.Models {
    public sealed class BlogModel : BaseModel {
        private const int PostsPerPage = 3;

        public int Page { get; private set; }

        public int PageCount { get; private set; }

        public bool IsFirstPage {
            get { return this.Page == 1; }
        }

        public int PostCount { get; private set; }

        public bool CanGoBack { get; private set; }

        public bool CanGoNext { get; private set; }

        public bool IsPageOutOfRange {
            get { return !this.IsFirstPage && this.PostCount == 0; }
        }

        public string NextUrl {
            get { return String.Concat(this.Content.Url, "?p=", this.Page + 1); }
        }

        public string BackUrl {
            get { return String.Concat(this.Content.Url, "?p=", this.Page - 1); }
        }

        public IEnumerable<BlogPostModel> Posts { get; private set; }

        public BlogModel(RenderModel model, int page) : base(model, noInit:true) {
            if (page < 1) {
                throw new ArgumentException("Cannot be less than 1", "page");
            }

            this.Page = page;

            this.Initialize();
        }

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);
            
            var posts = content.Descendants("blogPost").Where(x => x.GetPropertyValue("postDate", x.CreateDate) <= DateTime.Today).ToList();

            this.PageCount = (int) Math.Ceiling(posts.Count() / (double) PostsPerPage);

            this.Posts = posts
                .Select(x => new BlogPostModel(x, culture))
                .OrderByDescending(x => x.PostedOn)
                .Skip((this.Page - 1) * PostsPerPage)
                .Take(PostsPerPage);

            this.PostCount = this.Posts.Count();
            this.CanGoBack = this.Page > 1;
            this.CanGoNext = this.Page < this.PageCount;
        }
    }
}