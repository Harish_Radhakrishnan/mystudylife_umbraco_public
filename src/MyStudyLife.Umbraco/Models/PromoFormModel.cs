﻿using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyStudyLife.Umbraco.Models {
    public class PromoFormModel : CoreModel {
        public string CampusFormId { get; private set; }

        public PromoFormModel(IPublishedContent content, CultureInfo culture) : base(content, culture) { }

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            this.CampusFormId = content.GetPropertyValue<string>("campusFormId");
        }
    }
}