﻿using System.Globalization;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace MyStudyLife.Umbraco.Models {
    public sealed class TextModel : BaseModel {
        public HtmlString ContentHtml { get; private set; }

        public bool IsLegalText { get; private set; }

        public TextModel(RenderModel model) : base(model) { }

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            this.ContentHtml = content.GetPropertyValue<HtmlString>("content");
            this.IsLegalText = content.GetPropertyValue<bool>("isLegalText", false);
        }
    }
}