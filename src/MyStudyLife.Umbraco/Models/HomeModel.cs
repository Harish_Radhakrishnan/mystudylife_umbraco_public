﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using umbraco;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MyStudyLife.Umbraco.Models {
    public sealed class HomeModel : CoreModel {
        public string HeroScreenshot { get; private set; }

        public string PlatformWebScreenshot { get; private set; }

        public string PlatformPhoneScreenshot { get; private set; }

        public IReadOnlyCollection<ReviewModel> Reviews { get; private set; }

        public string GetTheAppText { get; private set; }

        public string GetTheAppUrl { get; private set; }

        public HomeModel(IPublishedContent content, CultureInfo culture) : base(content, culture) {}

        protected override void PopulateFromContent(IPublishedContent content, CultureInfo culture) {
            base.PopulateFromContent(content, culture);

            this.HeroScreenshot = content.GetPropertyValue<string>("heroScreenshot");
            this.PlatformWebScreenshot = content.GetPropertyValue<string>("platformsWebScreenshot");
            this.PlatformPhoneScreenshot = content.GetPropertyValue<string>("platformsPhoneScreenshot");

            if (!String.IsNullOrWhiteSpace(this.HeroScreenshot)) {
                this.HeroScreenshot += "?width=320";
            }
            if (!String.IsNullOrWhiteSpace(this.PlatformWebScreenshot)) {
                this.PlatformWebScreenshot += "?width=1096";
            }
            if (!String.IsNullOrWhiteSpace(this.PlatformPhoneScreenshot)) {
                this.PlatformPhoneScreenshot += "?width=340";
            }

            var reviews = content.Descendants("review").RandomOrder().Select(x => new ReviewModel(x)).ToList();

            this.Reviews = reviews.ToList();

            // TODO: utm_source on other URLs
            if (this.IsiPhoneOriPod) {
                this.GetTheAppText = "Get the iPhone app";
                this.GetTheAppUrl = this.iOSAppUrl;
            }
            else if (this.IsAndroid) {
                this.GetTheAppText = "Get the Android app";
                this.GetTheAppUrl = this.AndroidAppUrl;
            }
            else if (this.IsWindowsPhone) {
                this.GetTheAppText = "Get the Windows Phone app";
                this.GetTheAppUrl = this.WindowsPhoneAppUrl;
            }
            else {
                this.GetTheAppText = "Get Started";
                this.GetTheAppUrl = "http://app.mystudylife.com";
            }
        }
    }

    public class ReviewModel {
        public string Content { get; set; }

        public string ReviewerName { get; set; }

        public string ReviewSource { get; set; }

        public ReviewModel(IPublishedContent content) {
            this.Content = content.GetPropertyValue<string>("content");
            this.ReviewerName = content.GetPropertyValue<string>("reviewerName", "Anonymous");
            this.ReviewSource = content.GetPropertyValue<string>("reviewSource");
        }
    }
}