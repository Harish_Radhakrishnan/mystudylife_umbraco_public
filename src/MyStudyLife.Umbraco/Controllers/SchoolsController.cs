﻿using System.Web.Mvc;
using MyStudyLife.Umbraco.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using System.Web.Http;
using System.Net.Http;

namespace MyStudyLife.Umbraco.Controllers {
    public sealed class SchoolsController : RenderMvcController {
        public override ActionResult Index(RenderModel model) => View("~/Views/Schools.cshtml", new SchoolsModel(model.Content, model.CurrentCulture));
    }
}