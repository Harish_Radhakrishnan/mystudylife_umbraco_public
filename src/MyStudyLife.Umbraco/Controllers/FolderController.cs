﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public class FolderController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            if (model.Content.Children.Any()) {
                return new RedirectToUmbracoPageResult(model.Content.Children.First());
            }
            
            return base.Index(model);
        }
    }
}