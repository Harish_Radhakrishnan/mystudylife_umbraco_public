﻿using System.Web.Mvc;
using MyStudyLife.Umbraco.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public sealed class TourController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            return View("~/Views/Tour.cshtml", new TourModel(model.Content, model.CurrentCulture));
        }
    }

    public sealed class TourItemController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            return View("~/Views/Tour.cshtml", new TourModel(model.Content.Parent, model.CurrentCulture));
        }
    }
}