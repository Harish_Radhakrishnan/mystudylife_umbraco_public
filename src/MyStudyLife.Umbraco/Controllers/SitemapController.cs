﻿using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public class SitemapController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            var view = PartialView("~/Views/Partials/_Sitemap.cshtml", new RenderModel(model.Content.AncestorOrSelf(1), model.CurrentCulture));

            Response.ContentType = "text/xml";

            return view;
        }
    }
}