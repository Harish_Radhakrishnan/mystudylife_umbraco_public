﻿using System.Web.Mvc;
using MyStudyLife.Umbraco.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public sealed class HomeController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            return CurrentTemplate(new HomeModel(model.Content, model.CurrentCulture));
        }
    }
}