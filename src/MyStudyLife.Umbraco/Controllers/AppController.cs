﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public class AppController : Controller {
        private static readonly Regex iDeviceUserAgentRegex = new Regex(@"(?<!like )(?:iPhone|iPod)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex AndroidUserAgentRegex = new Regex(@"(?<!like )(?:Android)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private const string WebAppUrl = "https://app.mystudylife.com";

        public ActionResult DoRedirect() {
            var homeNode = global::Umbraco.Web.UmbracoContext.Current.Application.Services.ContentService.GetByLevel(1).First();

            string url = WebAppUrl;
            string userAgent = Request.UserAgent;

            if (userAgent != null) {
                // In the most recent versions of Windows Phone 8.1, WP has dropped the "like" from
                // "like Android" (although still has "like iPhone").
                if (userAgent.Contains("Windows Phone")) {
                    url = homeNode.GetValue<string>("windowsPhoneAppUrl");
                }
                else if (iDeviceUserAgentRegex.IsMatch(userAgent)) {
                    url = homeNode.GetValue<string>("iosAppUrl");
                }
                else if (AndroidUserAgentRegex.IsMatch(userAgent)) {
                    url = homeNode.GetValue<string>("androidAppUrl");
                }
            }

            return new RedirectResult(url, false);
        }
    }
}
