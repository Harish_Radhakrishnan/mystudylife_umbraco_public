﻿using System.Web.Mvc;
using MyStudyLife.Umbraco.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public sealed class ErrorController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            return CurrentTemplate(new BaseModel(model));
        }
    }
}