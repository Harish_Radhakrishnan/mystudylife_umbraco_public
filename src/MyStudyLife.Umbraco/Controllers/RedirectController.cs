﻿using System;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public class RedirectController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            var redirectToUrl = model.Content.GetPropertyValue<string>("redirectToUrl");

            if (String.IsNullOrWhiteSpace(redirectToUrl)) {
                return base.Index(model);
            }
            
            return new RedirectResult(redirectToUrl, model.Content.GetPropertyValue<bool>("permanent"));
        }
    }
}