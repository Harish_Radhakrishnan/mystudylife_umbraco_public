﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ClientDependency.Core;
using MyStudyLife.Umbraco.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MyStudyLife.Umbraco.Controllers {
    public sealed class BlogController : RenderMvcController {
        public override ActionResult Index(RenderModel model) {
            var pageStr = Request.QueryString["p"];

            int page;

            if (String.IsNullOrWhiteSpace(pageStr)) {
                page = 1;
            }
            else if (!Int32.TryParse(pageStr, out page) || page < 1) {
                var queryString = Request.QueryString;

                string url = "/blog";

                if (queryString.Count > 1) {
                    url += ToQueryStringWithoutPage(queryString).TrimEnd('?', '&');
                }

                return Redirect(url);
            }

            var blogModel = new BlogModel(model, page);

            if (blogModel.IsPageOutOfRange) {
                return Redirect("/blog" + ToQueryStringWithoutPage(Request.QueryString) + "p=" + blogModel.PageCount);
            }

            return CurrentTemplate(blogModel);
        }

        private static string ToQueryStringWithoutPage(NameValueCollection queryString) {
            var queryBuilder = new StringBuilder("?");

            for (int i = 0; i < queryString.Count; i++) {
                var key = queryString.GetKey(i);
                var values = queryString.GetValues(key);

                if (key != "p" && values != null) {
                    foreach (var value in values) {
                        queryBuilder.Append(HttpUtility.UrlEncode(key)).Append("=").Append(HttpUtility.UrlEncode(value)).Append("&");
                    }
                }
            }

            return queryBuilder.ToString();
        }
    }
}