﻿/// <binding AfterBuild='min' Clean='clean' ProjectOpened='watch:js' />
var gulp = require('gulp'),
    print = require('gulp-print'),
    merge = require('merge-stream'),
    concat = require('gulp-concat'),
    clone = require('gulp-clone'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    cssnano = require('gulp-cssnano'),
    less = require('gulp-less'),
    del = require('del');

var config = {
    root: './'
};

config.js = [
    {
        taskName: 'min:all:js',
        src: [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/hammerjs/hammer.js',
            config.root + 'js/libs/*.js',
            config.root + 'js/**/*.js',
            '!' + config.root + 'js/bundle.js',
            '!' + config.root + 'js/bundle.min.js'
        ],
        dest: config.root + 'js/bundle.'
    }
];

config.less = [
    {
        src: config.root + 'less/main.less',
        dest: config.root + 'css/main.min.css'
    }
];

config.lessOutput = config.root + 'css/*.min.css';

gulp.task('clean:js', function () {
    return del(config.js.map(function (file) { return file.dest + '*'; }));
});

gulp.task('clean:css', function () {
    return del([config.lessOutput]);
});
gulp.task('clean', ['clean:js', 'clean:css']);

config.js.forEach(function (file) {
    gulp.task(file.taskName, function () {
        var js = gulp.src(file.src)
                .pipe(sourcemaps.init());

        js = js.pipe(concat(file.dest + 'js'));

        var min = js.pipe(clone())
            .pipe(uglify())
            .pipe(rename(file.dest + 'min.js'));

        return merge(js, min)
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(''));
    });
});

gulp.task('min:js', ['clean:js'], function (done) {
    var count = config.js.length;
    var finishedCount = 0;
    var taskNames = config.js.map(function (file) {
        return file.taskName;
    })

    var tasks = gulp.start(taskNames);

    function finished(event) {
        if (taskNames.indexOf(event.task) > -1) {
            finishedCount++;
        }
        if (finishedCount === count) {
            gulp.removeListener('task_stop', finished);
            return done();
        }
    }

    gulp.on('task_stop', finished);
});

gulp.task('min:less', ['clean:css'], function () {
    return merge(config.less.map(function (file) {
        return gulp.src(file.src)
            .pipe(less())
            .pipe(cssnano({
                zindex: false,
                discardComments: {
                    removeAll: true
                },
                // Fixes issue where this renames @keyframes animations which breaks IE11 / Windows 7
                reduceIdents: {
                    keyframes: false
                }
            }))
            .pipe(rename(file.dest))
            .pipe(gulp.dest(''));
    }));
});
gulp.task('min', ['min:js', 'min:less']);

gulp.task('min:less', ['clean:css'], function () {
    return merge(config.less.map(function (file) {
        return gulp.src(file.src)
            .pipe(less())
            .pipe(cssnano({
                zindex: false,
                discardComments: {
                    removeAll: true
                }
            }))
            .pipe(rename(file.dest))
            .pipe(gulp.dest(''));
    }));
});
gulp.task('min', ['min:js', 'min:less']);

gulp.task('watch:js', function (cb /* never called */) {
    config.js.forEach(function (file) {
        var toWatch = file.src.concat('!' + file.dest + '*');

        var watcher = gulp.watch(toWatch, [file.taskName]);
        watcher.on('change', function (event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        });
    });
});